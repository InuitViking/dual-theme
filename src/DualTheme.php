<?php

namespace Imms\DualTheme;

use Imms\Classes\Bootstrapper;

class DualTheme {

    // Required
    public array $subscribedPaths = [];
    public array $subscribedEvents = [
        'page_load',
        'page_end',
        'themeChangeButton'
    ];

    private array $immsConfig;
    private string $currentTheme;

    private bool $isButtonRendered = false;
    private string $svg =   '<svg id="Icons" xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 32 32" xml:space="preserve">
                                <style type="text/css">
                                    .st0{fill:none;stroke:#000000;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;}
                                    .st1{fill:none;stroke:#000000;stroke-width:2;stroke-linejoin:round;stroke-miterlimit:10;}
                                </style>
                                <line class="st0" x1="16" y1="3" x2="16" y2="29"/>
                                <path class="st0" d="M16,23c-3.87,0-7-3.13-7-7s3.13-7,7-7"/>
                                <line class="st0" x1="6.81" y1="6.81" x2="8.93" y2="8.93"/>
                                <line class="st0" x1="3" y1="16" x2="6" y2="16"/>
                                <line class="st0" x1="6.81" y1="25.19" x2="8.93" y2="23.07"/>
                                <path class="st0" d="M16,12.55C17.2,10.43,19.48,9,22.09,9c0.16,0,0.31,0.01,0.47,0.02c-1.67,0.88-2.8,2.63-2.8,4.64
                                                    c0,2.9,2.35,5.25,5.25,5.25c1.6,0,3.03-0.72,3.99-1.85C28.48,20.43,25.59,23,22.09,23c-2.61,0-4.89-1.43-6.09-3.55"/>
                            </svg>';

    private string $themeChangeButton;

    public function __construct() {
        $this->immsConfig = Bootstrapper::getIni();
        $this->currentTheme = $this->getCurrentTheme();
        $this->themeChangeButton = "<a class='themeChangeButton' style='display: inline-block; width: 50px;' href='?IMMSTheme={$this->getOppositeTheme()}'>{$this->svg}</a>";
    }

    public function getNotification (string $event): void {
        // At page load
        if ($event === $this->subscribedEvents[0]) {
            $path = ltrim(urldecode($_SERVER['REQUEST_URI']),'?');
            $path = substr($path, 0, strpos($path, '?'));

            if (isset($_GET['refreshTheme'])) {
                // IMMS apparently needs this one-second sleep for the change to take effect.
                sleep(1);
                header("Location: $path");
            }

            if (isset($_GET['IMMSTheme'])) {
                if (isset($_COOKIE['IMMSTheme'])) {
                    unset($_COOKIE['IMMSTheme']);
                }
                setcookie("IMMSTheme", $_GET['IMMSTheme']);
                $this->currentTheme = $_GET['IMMSTheme'];
                header("Location: $path?refreshTheme=1");
            } else {
                setcookie("IMMSTheme", $this->currentTheme);
            }
        }

        // Where a theme has decided to put them theme change button
        if ($event === $this->subscribedEvents[2]) {
            $this->isButtonRendered = true;
            echo $this->themeChangeButton;
        }

        // At page end
        if ($event === $this->subscribedEvents[1] && $this->isButtonRendered === false) {
            echo $this->themeChangeButton;
        }
    }

    private function getCurrentTheme (): string {
        return $_COOKIE['IMMSTheme'] ?? $this->immsConfig['app']['theme'];
    }

    private function getOppositeTheme (): string {
        if ($this->currentTheme === $this->immsConfig['dual-theme']['primary']) {
            return $this->immsConfig['dual-theme']['secondary'];
        } elseif ($this->currentTheme === $this->immsConfig['dual-theme']['secondary']) {
            return $this->immsConfig['dual-theme']['primary'];
        }

        return $this->currentTheme;
    }

}